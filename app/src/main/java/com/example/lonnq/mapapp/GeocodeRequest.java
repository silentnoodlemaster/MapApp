package com.example.lonnq.mapapp;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GeocodeRequest extends AsyncTask<String, Void, String>
{
    private static final String TAG = "GeocodeRequest";
    private String getCoordinates(String jsonString)
    {
        try {
            JSONObject location = new JSONObject(jsonString)
                    .getJSONArray("results")
                    .getJSONObject(0)
                    .getJSONObject("geometry")
                    .getJSONObject("location");

            return (location.getDouble("lat")+","+ location.getDouble("lng"));
        }

        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

        return null;
    }


    @Override
    protected String doInBackground(String[] params)
    {
        StringBuilder jsonString = new StringBuilder();

        try
        {
            URL url = new URL(params[0]);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String nextLine;

            while((nextLine = in.readLine()) != null)
            {
                jsonString.append(nextLine);
            }

            return getCoordinates(jsonString.toString());
        }

        catch (Exception e)
        {
            Log.e(TAG, e.getMessage());
        }

        return null;
    }
}
