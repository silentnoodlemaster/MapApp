package com.example.lonnq.mapapp;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    ListView lv;
    ArrayList<Route> routes = new ArrayList<>();
    FileIO fio = new FileIO(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            routes = fio.fileIn();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = getIntent();
        Route route = (Route) intent.getSerializableExtra("route");
        if (route != null) {
            routes.add(route);
        }
        fio.fileOut(routes);
        lv = findViewById(R.id.routeList);
        ArrayAdapter<Route> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, routes);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                intent.putExtra("route", routes.get(position));
                startActivity(intent);
            }
        });
    }

    public void onClick(View v) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);

    }
}
