package com.example.lonnq.mapapp;

public class Waypoint {
    private int[] waypointOrder;
    private int distance;

    public void setWaypointOrder(int[] i){
        this.waypointOrder = i;
    }
    public void setDistance(int i) {
        this.distance = i;
    }
    public int[] getWaypointOrder() {
        return waypointOrder;
    }
    public int getDistance() {
        return distance;
    }
}
