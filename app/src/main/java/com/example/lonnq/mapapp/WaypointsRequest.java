package com.example.lonnq.mapapp;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WaypointsRequest extends AsyncTask<String, Void, Waypoint>{


    private int[] getOrder(String jsonString) {
        try {
            JSONArray waypointResult =  new JSONObject(jsonString)
                    .getJSONArray("routes")
                    .getJSONObject(0)
                    .getJSONArray("waypoint_order");
            int[] waypointOrder = new int[waypointResult.length()];
            for(int i=0;i<waypointResult.length();i++) {
                waypointOrder[i] = waypointResult.getInt(i);
            }
            return waypointOrder;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
    private int getDistance(String jsonString) {
        int distance = 0;
        try {
            JSONArray waypointLegs =  new JSONObject(jsonString)
                    .getJSONArray("routes")
                    .getJSONObject(0)
                    .getJSONArray("legs");

            for(int i = 0; i<waypointLegs.length(); i++) {
                distance += waypointLegs.getJSONObject(i)
                        .getJSONObject("distance")
                        .getInt("value");
            }

            return distance;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }
    @Override
    protected Waypoint doInBackground(String... strings) {
        StringBuilder jsonString = new StringBuilder();

        try
        {
            URL url = new URL(strings[0]);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String nextLine;

            while((nextLine = in.readLine()) != null)
            {
                jsonString.append(nextLine);
            }
            Waypoint waypoint = new Waypoint();
            waypoint.setWaypointOrder(getOrder(jsonString.toString()));
            waypoint.setDistance(getDistance(jsonString.toString()));
            return waypoint;
        }

        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
