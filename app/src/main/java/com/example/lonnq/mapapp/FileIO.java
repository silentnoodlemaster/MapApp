package com.example.lonnq.mapapp;

import android.content.Context;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class FileIO{
    MainActivity ma;
    FileIO(MainActivity ma) {
        this.ma = ma;
    }
    public void fileOut(ArrayList<Route> data) {
        try {
            FileOutputStream fOut = ma.openFileOutput("routes.ser",Context.MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fOut);
            out.writeObject(data);
            out.close();
            fOut.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }
    public ArrayList<Route> fileIn() {
        ArrayList<Route> routes = new ArrayList<>();
        try {
            FileInputStream fileIn = ma.openFileInput("routes.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            routes = (ArrayList<Route>) in.readObject();
            in.close();
            fileIn.close();

        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Note class not found");
            c.printStackTrace();
        }
        return routes;
    }
}
