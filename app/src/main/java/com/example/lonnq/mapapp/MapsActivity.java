package com.example.lonnq.mapapp;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    LatLng startPosition;
    int distance;
    ArrayList<String> waypoints = new ArrayList<>();
    ArrayList<Marker> markers = new ArrayList<>();
    Route route = null;
    EditText startText, nameText;
    TextView distanceText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Intent intent = getIntent();
        route = (Route) intent.getSerializableExtra("route");
        startText = findViewById(R.id.startPoint);
        nameText = findViewById(R.id.routeName);
        distanceText = findViewById(R.id.distance);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_cool));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(12f));

        if (route != null) {
            nameText.setText(route.getName());
            setStart(route.getStartingPoint().latitude+"%2C"+route.getStartingPoint().longitude);
            addWaypoints(route.getWaypoints());
            distanceText.setText(String.valueOf(route.getDistance()+ " m"));
        }


    }
    public void onSet(View v) {

        String start = startText.getText().toString();
        setStart(start);

    }
    private void setStart(String start) {
        String url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + start + "&key=AIzaSyCP0Bknoy7uBpLgJDmnh6coM0shCI_debE";
        try {
            String[] startPositionstrings = new GeocodeRequest().execute(url).get().split(",");
            startPosition = new LatLng(Double.parseDouble(startPositionstrings[0]),Double.parseDouble(startPositionstrings[1]));
            mMap.addMarker(new MarkerOptions().position(startPosition));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(startPosition));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void onAdd(View v) {
        EditText waypointText = findViewById(R.id.waypoint);
        String waypoint = waypointText.getText().toString();
        String url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + waypoint + "&key=AIzaSyCP0Bknoy7uBpLgJDmnh6coM0shCI_debE";
        try {
            String way = new GeocodeRequest().execute(url).get();
            if (way != null) {
                waypoints.add(way);
                addWaypoints(waypoints);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    public void addWaypoints(ArrayList<String> waypoints) {
        StringBuilder waypointStringBuilder = new StringBuilder();
        String separator = "%7C";
        for (String wayp : waypoints) {
            waypointStringBuilder.append(wayp);
            waypointStringBuilder.append(separator);
        }
        String waypointString = waypointStringBuilder.toString();
        waypointString = waypointString.substring(0,waypointString.length()-separator.length());
        String waypointURL = "https://maps.googleapis.com/maps/api/directions/json?origin="+startPosition.latitude+","+startPosition.longitude+"&destination="+startPosition.latitude+","+startPosition.longitude+"&waypoints=optimize:true|"+ waypointString +"&key=AIzaSyCP0Bknoy7uBpLgJDmnh6coM0shCI_debE";
        Waypoint wp = null;
        try {
            wp = new WaypointsRequest().execute(waypointURL).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        int[] waypointOrder = wp.getWaypointOrder();
        distance = wp.getDistance();
        distanceText.setText(String.valueOf(distance)+ " m");

        for (Marker marker : markers) {
            marker.remove();
        }

        for(int i = 0; i<waypoints.size(); i++) {
            String[] waystrings = waypoints.get(i).split(",");
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng( Double.parseDouble(waystrings[0]),Double.parseDouble(waystrings[1])))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                    .title(String.valueOf(waypointOrder[i]+1)));
            marker.showInfoWindow();
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng( Double.parseDouble(waystrings[0]),Double.parseDouble(waystrings[1]))));
            markers.add(marker);
        }

    }


    public void onSave(View v){
        String name = nameText.getText().toString();
        Route route = new Route(name,startPosition, waypoints, (double) distance);
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("route", route);
        startActivity(intent);
    }
}
