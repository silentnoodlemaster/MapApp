package com.example.lonnq.mapapp;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;

class Route implements Serializable {
    private String name;
    private double startingPointLatitude;
    private double getStartingPointLongiude;
    private ArrayList<String> waypoints;
    private Double distance;
    Route(String name, LatLng startingPoint, ArrayList<String> waypoints,Double distance) {
        this.name = name;
        this.startingPointLatitude = startingPoint.latitude;
        this.getStartingPointLongiude = startingPoint.longitude;
        this.waypoints = waypoints;
        this.distance = distance;
    }
    public String getName() {
        return name;
    }

    public LatLng getStartingPoint() {
        return new LatLng(startingPointLatitude,getStartingPointLongiude);
    }

    public ArrayList<String> getWaypoints() {
        return waypoints;
    }

    public Double getDistance() {
        return distance;
    }

    @Override
    public String toString(){
        return this.name + ": " + String.valueOf(distance)+ " m";
    }

}
